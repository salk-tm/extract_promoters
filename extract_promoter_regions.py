#!/usr/bin/env python3
from BCBio import GFF
import pandas
import argparse
import sys


def load_gff3(fpath):
    """ loads features from a gff3 file and returns a list of seqrecord objects
    """
    with open(fpath) as fin:
        ret = []
        for rec in GFF.parse(fin):
            ret.append(rec)
    return ret


def get_features(seq_record, feat_type):
    """ extract features/sub_features from seq_record that match feat_type
    """
    to_process = [f for f in seq_record.features]
    genes = []
    while(len(to_process) > 0):
        f = to_process.pop()
        if(f.type == feat_type):
            genes.append(f)
        for s in f.sub_features:
            to_process.append(s)
    return genes


def promoter_bed_info(seq_id, gene_feature, prelen, postlen, about_end):
    """ given a sequence id, a feature, and a region around the features start
        to extract, return a tuple representing bed format records for the
        region of interest
    """
    loc = gene_feature.location
    if(about_end):
        if(loc.strand == -1):
            return (seq_id, loc.start - postlen, loc.start + prelen, gene_feature.id, ".","-")
        else:
            return (seq_id, loc.end - prelen, loc.end + postlen, gene_feature.id, ".", "+")
    else:
        if(loc.strand == -1):
            return (seq_id, loc.end - postlen, loc.end + prelen, gene_feature.id, ".","-")
        else:
            return (seq_id, loc.start - prelen, loc.start + postlen, gene_feature.id, ".", "+")


def stitched_cds_approach(seq_id, gene_feature, prelen, postlen, about_end):
    """ identify the start of the cds features associated with gene_feature,
        and extract region around that start of CDS (translation start site)
        if about_end is true, extract region from end of cds features instead
        of start. Returns None if no CDS features are found
    """
    cds_features = []
    todo = [gene_feature]
    while(len(todo) > 0):
        f = todo.pop()
        if(f.type == "CDS"):
            cds_features.append(f)
        for s in f.sub_features:
            todo.append(s)
    if(len(cds_features) == 0):
        return None
    start = min(f.location.start for f in cds_features)
    end = max(f.location.end for f in cds_features)
    if(about_end):
        if(gene_feature.location.strand == -1):
            return (seq_id, start - postlen, start + prelen, gene_feature.id, ".","-")
        else:
            return (seq_id, end - prelen, end + postlen, gene_feature.id, ".", "+")
    else:
        if(gene_feature.location.strand == -1):
            return (seq_id, end - postlen, end + prelen, gene_feature.id, ".","-")
        else:
            return (seq_id, start - prelen, start + postlen, gene_feature.id, ".", "+")


def get_args():
    parser = argparse.ArgumentParser(
        description=(
            "extracts promoter regions as bed format from a gff3 file "
            "containing genes. Writes bed data to stdout. Works by extracting "
            "around either the start or end of features of the specified type. "
        )
    )
    parser.add_argument(
        "gff3",
        help="a gff3 file with genes you wish to get promoter regions for"
    )
    parser.add_argument(
        "-f", "--feature_type",
        default="gene",
        help="The feature type to extract regions around. Default = 'gene'"
    )
    parser.add_argument(
        "-e", "--end",
        default=False,
        action='store_true',
        help="If present, extract region from around end of feature rather than start"
    )
    parser.add_argument(
        "-t", "--tss",
        default=True,
        action='store_true',
        help="if present extract region from around start of translation start site for gene features"
    )
    parser.add_argument(
        "-u", "--upstream",
        default=500,
        type=int,
        help="length upstream from start of gene to extract. Default = 500"
    )
    parser.add_argument(
        "-d", "--downstream",
        default = 0,
        type=int,
        help="length downstream from start of gene to extract. Default = 0"
    )
    return parser.parse_args()


def main():
    args = get_args()
    gff = load_gff3(args.gff3)
    genes = {r.id : [g for g in get_features(r, args.feature_type)] for r in gff}
    if(args.tss):
        bed_data = [
            stitched_cds_approach(seq_id, f, args.upstream, args.downstream, args.end)
            for seq_id, genes in genes.items() for f in genes
        ]
        bed_data = pandas.DataFrame([e for e in bed_data if e is not None])
    else:
        bed_data = pandas.DataFrame([
            promoter_bed_info(seq_id, f, args.upstream, args.downstream, args.end)
            for seq_id, genes in genes.items() for f in genes
        ])
    bed_data[1] = bed_data[1].clip(lower=0)
    
    bed_data.to_csv(
        sys.stdout, sep="\t", header=None, index=None
    )


if(__name__ == "__main__"):
    main()
