# extract_promoters
Tool for extracting promoter regions from a fasta file using gene models found in a gff3 file.

# Installation
The script that follows is a minimal install that will let you download and run this script in a new environment on linux

```
git clone https://gitlab.com/salk-tm/extract_promoters.git
cd extract_promoters
conda env create -n prom -f requirements.yml
conda activate prom
ln -s $(pwd)/extract_promoter_regions.py $(dirname $(which python))/extract_promoter_regions.py
```

# Usage
The script that follows demonstrates a common workflow for extracting promoter regions.

```
extract_promoter_regions.py some.gff3 > some.promoters.bed
bedtools getfasta -name -s -bed some.promoters.bed -fi some.fasta -fo some.promoters.fasta
```
